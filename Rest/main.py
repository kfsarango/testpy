import requests
import json

def queryAllDataMerlot(tipo):

    res = requests.get('http://127.0.0.1:4567/getmerlot'+str(tipo))

    if res.status_code != 200:
        # algo salio mal
        raise ApiError('GET /getmerlot/ {}'.format(resp.status_code))


    data_string = json.dumps(res.json())

    return json.loads(data_string)


def main():
	with open('dataScrapy.json', 'w') as outfile:
		json.dump(queryAllDataMerlot(1), outfile)

	print('Data Saved')


if __name__ == '__main__':
	main()